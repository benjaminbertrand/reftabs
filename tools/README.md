Tools
--

# Convert .json to a table
* http://json2table.com/

# Check the .json syntax
* https://jsonformatter.curiousconcept.com/
