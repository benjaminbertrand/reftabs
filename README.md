# ESS Reference & Lookup Tables

----

- [ Project Directory Structure ](#Directory)
- [ Tag Versioning ](#Tag)

----

## Project Directory Structure
<a name="Directory"></a>

### [init](init)

It loads during the initialization and contains ESS constant variables and enumerations for:
- [databuffer-ess](init/databuffer-ess.json) proton beam modes, destination and state according to ESS-0038258 (_Description of Modes for ESS Accelerator Operation_)

- [mevts-ess](init/mevts-ess.json) - master events according to: ESS-1837307 (_Time Structure of the Proton Beam Pulses in the LINAC_).

See [Beam Configuration](https://confluence.esss.lu.se/display/ABC/Beam+Configuration) for additional details.

### [supercycles](supercycles)

It loads upon the selection by an ESS operator (see the [manual](https://confluence.esss.lu.se/display/HAR/Supecycle)). The dynamic routines/cycles (runtime).

### [tools](tools)

Productivity enhancements.

### [doc](doc)

Other documents, manuals, references and drafts.

## Tag Versioning
<a name="Tag"></a>

- MAJOR (1st digit) == {Backward compatibility mark}
- MINOR (2sd digit) == {ESS-0038258 Chess Revision, ProtNum}
- PATH (3rd digit) == {ESS-1837307 Chess Revision, ProtVer}
- BUILD (4th digit) == {supercycle adds, bug corrections}

e.g. 1.5.1+1 means:
- all with 1 is compatible,
- ProtNum == 5, ESS-0038258 revision == 5,
- ProtVer == 1, ESS-1837307 revistion == 1,
- +1 - some files were introduced, e.g. supercycle tables.
