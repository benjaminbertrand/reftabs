# Changelog



## Version 1.6.1 (2020-04-08)


### Bug Fixes
- PBMod updated according to Table 5 of ESS-0038258 revision 6.
- PBDest refers to Table 8 of ESS-0038258 revision 6.
- DTL1 is removed from NCL set because DTL2FC is sufficient.
- DTL1 is removed from NCL set because DTL2FC is sufficient.




### Documentation Changes
- Changelog added.





## Version 1.5.1+1 (2020-03-25)









## Version 1.5.1 (2020-03-24)









## Version 1.5.0 (2020-03-17)









